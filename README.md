Remove this window ‘File name prefix’ and ‘File counter’
Please add new tab
1.	 Add Number Counter For File Name (for nesting has more than one sheet)
It is selected by default.
Example: 
Let say the user made a Nesting has 3 sheets and gave it file name (star)then we will take 3 mpg files with these names.
star1.mpg 
star2.mpg
star3.mpg

2. Exclude First File Name From Counting
This box is unchecked by default.If this box is checked then we will have 3 files the first one is exactly what they user wrote in the save box,the other 2 files are numbered.
  Example: 
star.mpg 
star1.mpg
star2.mpg

3.	Start Counting File Name From Number

This box is empty by default.But if we write a number 8 for example thats means the counter will start the from number 8 like below;
star8.mpg 
star9.mpg
star10.mpg

4.	Add Symbol Between File Name and Numbers
This box is empty by default.But if we put a symbol any caracter,number or text for example 

Example1: 
If we put (-) then the file names will be like below.
star-1.mpg 
star-2.mpg
star-3.mpg

Example2:
If we put (-00) then the file names will be like below.
star-001.mpg
star-002.mpg
star-003.mpg
Example3:
If we put (wars) then the file names will be like below

starwars1.mpg
starwars2.mpg
starwars3.mpg

Simply it is  (file name ) (any character) (counter number)

YES.
If the Exclude First File Name From Counting is checked the condition will be applied.
